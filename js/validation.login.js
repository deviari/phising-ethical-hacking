//jQuery time
// https://www.hackandphp.com/blog/gmail-style-clone-login-page-and-validation-using-php-jquery-ajax-and-bootstrap
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches

$(".next").click(function() {
    current_fs = $(this).parent();

    var fieldsetValue = $("fieldset").index(current_fs);
    if (fieldsetValue == 0) {
        $('#password').val('');
        $('#msg').text('');
        var email = $('#email').val();        
        if (email) {
            next_fs = current_fs.next();
            animate(next_fs);        
        }
    }
    if (fieldsetValue == 1) {

        next_fs = current_fs.next();
        animate(next_fs);


    }

});

function animate(next_fs) {
    console.log(animating);
    if (animating) return false;
    animating = true;
    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
    //show the next fieldset

    next_fs.show();
    $(".login").animate({
        height: "350px"
    });
    //hide the current fieldset with style
    current_fs.animate({
        opacity: 0
    }, {
        step: function(now, mx) {

            left = (now * 10) + "%";

            opacity = 1 - now;

            current_fs.css({
                'transform': 'scale(' + scale + ')'
            });
            next_fs.css({
                'left': left,
                'opacity': opacity
            });
            next_fs.css({
                'margin-left': '95px'
            });
        },
        duration: 200,
        complete: function() {
            current_fs.hide();
            animating = false;
        },

        easing: 'easeInOutBack'
    });

}


$(".previous").click(function() {
    if (animating) return false;
    animating = true;

    current_fs = $(this).parent();
    previous_fs = $(this).parent().prev();

    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

    //show the previous fieldset
    previous_fs.show();
    $(".login").animate({
        height: "300px"
    });
    $('#error_msg').text('');
    //hide the current fieldset with style
    current_fs.animate({
        opacity: 0
    }, {
        step: function(now, mx) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale previous_fs from 80% to 100%
            scale = 0.8 + (1 - now) * 0.2;
            //2. take current_fs to the right(50%) - from 0%
            left = ((1 - now) * 20) + "%";
            //3. increase opacity of previous_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.css({
                'left': left
            });

            previous_fs.css({
                'transform': 'scale(' + scale + ')',
                'opacity': opacity
            });

        },
        duration: 200,
        complete: function() {
            current_fs.hide();
            animating = false;
        },
        //this comes from the custom easing plugin
        easing: 'easeInOutBack'
    });
});

$('form input').keydown(function (e) {
if (e.keyCode == 13) {
    e.preventDefault();
    return false;
}
});